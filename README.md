# Bakalářská práce
Autor: Michal Rudolf\
Téma: Zásuvné moduly pro kreativní práce v programech Blender a Photoshop\
Vedoucí: Ing. Chludil

## Obsah prace.zip
- API_tests				- jednoduché testovací moduly různých API
    - SDK_hello_world			- testovací modul Photoshop SDK API se zdrojovými kódy (Visual Studio projekt)
    - UXP_hello_world			- testovací modul Adobe UXP API
    - blender_hello_world.py		- testovací modul Blender Python API
- text					- text bakalářské práce
    - thesis.pdf			- text práce ve formátu PDF
    - src				- zdrojový kód práce v LaTeX
- plugins					- výsledné moduly
	- import_render_passes.jsx	- zásuvný modul pro Photoshop
    - render_pass_export.py		- zásuvný modul pro Blender
- resources				- zdroje z průzkumu
	- surveys.pdf			- písemná korespondence s umělci
	- suggestions.pdf			- návrhy na zásuvné moduly od respondenta č. 4
